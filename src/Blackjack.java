import java.util.Scanner;

public class Blackjack{

		public static Deck myDeck = new Deck(1, true);
		//public static Player me = new Player("PLAYER_1");
		public static Player dealer = new Player("Dealer");
		public static final int BLACKJACK = 21;
		public static int nPlayers;
		public static Scanner sc = new Scanner(System.in);

		// public static ArrayList<Player> players = new ArrayList<Players>();

	public static void main(String[] args){

		getPlayers();

		// Deal the cards
		// TODO: This should probably be done as a list that iterates through all players till each player has 2 cards
		me.addCard(myDeck.dealNextCard());
		dealer.addCard(myDeck.dealNextCard());
		me.addCard(myDeck.dealNextCard());
		dealer.addCard(myDeck.dealNextCard());


		System.out.println("Hands dealt");

		me.printHand(true);
		dealer.printHand(false);
		System.out.println("\n");

		// Flags

		boolean meDone = false;
		boolean dealerDone = false;
		String ans;

		while(!meDone || !dealerDone){
			// PLAYER
			if(!meDone){
				// Hand sum < 21 so valid hand
				while(me.getHandSum() <= BLACKJACK){
					System.out.println("HIT or STAY? ('H'or 'S'):");
					ans = sc.next();
					System.out.println("\n");

					// If hit
					if(ans.compareToIgnoreCase("H") == 0){
						// Give me a card
						me.addCard(myDeck.dealNextCard());
						// Print my hand
						me.printHand(true);
					}
						else{
							meDone = true;
							break;
						}
				}
				meDone = true;

			}
			// DEALER
			if(!dealerDone){
				// Dealer hits on 17
				if(dealer.getHandSum() < 17){
					System.out.println("Dealer must hit.\n");
					dealerDone = !dealer.addCard(myDeck.dealNextCard());
					dealer.printHand(true);
				}
				else{
					System.out.println("Dealer must stay.\n");
					dealerDone = true;
				}
			}

		}
		// Done with scanner
		sc.close();


		// Print hands
		System.out.println("\n");
		me.printHand(true);
		dealer.printHand(true);

		// Get hand sums
		// TODO: Go through  list of players and compare their hand to dealer
		int mySum = me.getHandSum();
		int dealerSum = dealer.getHandSum();

		// WIN CONDITIONS
		if(mySum > dealerSum && mySum <= 21 || dealerSum > 21){
			System.out.println("You win " + me.getPlayer());
		}
		else if(dealerSum > mySum && dealerSum <= 21 || mySum > 21){
			System.out.println("You lose " + me.getPlayer());
		}
		else if(mySum == dealerSum){
			System.out.println("DRAW!!!");
		}
	}

	public static void getPlayers(){
		boolean havePlayerNum = false;
		System.out.println("How many players are there? (1-4)");
		while(!havePlayerNum){
			// Initialisation
			try{
				nPlayers = Integer.parseInt(sc.next());
				havePlayerNum = true;
				System.out.println("DONT FORGET PLAYER NAMES");
			}
			catch (Exception e){
				System.err.println("Please enter numerical value between 1 and 4 for number of players \n\n");
			}

			Player[] players = new Player[nPlayers];
			for(int p = 0; p < nPlayers; p++ ){
				int _p = p + 1;
				System.out.println("What is the name of player " + _p + "?");
				String ans = sc.next();

				players[p] = new Player(ans);
			}

		}
	}
}
