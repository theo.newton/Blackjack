/**
* An implementation of a card suit
*
* @author Theo Newton 
*
* @date 12 Oct 2015
*/
public enum Suit {
	Clubs,
	Diamonds,
	Spades,
	Hearts,
}