/**
* An implementation of the players including the dealer
*
* @author Theo Newton 
*
* @date 12 Oct 2015
*/
public class Player{
	private Blackjack bj;
	private String name;
	// number of cards in the player's hand
	private int numCards;
	private static final int MAXCARDS = 5;

	// Since not dynamic we must specify a max hand size
	private Card[] hand = new Card[MAXCARDS];

	/**
	 * Constructor for a player which takes in the player's name
	 * 
	 * @param n The name of the player
	 */
	public Player(String n){
		this.name = n;
		// Make player's hand empty
		this.emptyHand();
	}

	/**
	*	Add a card to the player's hand
	*
	*	@param c The card to add
	*	@return Hand is not a bust
	*/
	public boolean addCard(Card c){
		// print error message if at max cards
		
		// Should change this to a win condition if handSum <= 21
		if(this.numCards == MAXCARDS){
			System.err.printf("%s's hand already has the maximum amount cards. No more cards may be added\n", this.name);
			return false;
			//System.exit(1);
		}

		// Add new card and increment the counter
		this.hand[this.numCards] = c;
		this.numCards++;
		return (this.getHandSum() <= bj.BLACKJACK);
	}
	

	public String getPlayer(){
		return name;
	}
	public Player[] p;

	/**
	*	Get the sum of the cards in the hand 
	*/
	public int getHandSum(){
		int handSum = 0;
		int cardNum;
		int numAces = 0;

		// calculate card sum
		for(int c = 0; c < this.numCards; c++){
			cardNum = this.hand[c].getNum();
			if(cardNum == 1){
				numAces ++;
				handSum += 11;
			}
			else if(cardNum > 10)
				handSum += 10;
			else
				handSum += cardNum;
		}

		// conversion of aces into 11 -> 1
		while(handSum > 21 && numAces > 0){
			handSum -= 10;
			numAces--;
		}

		return handSum;
	}

	/**
	*	Print out the hand
	*	@param showFirstCard whether or not first card should be displayed
	*/
	public void printHand(boolean showFirstCard){
		System.out.printf("%s's cards: \n", this.name);
		for(int c = 0; c < this.numCards; c++){
			if (c== 0 && !showFirstCard){
				System.out.println(" [HIDDEN]");
			}
			else{
				System.out.printf(" %s\n", this.hand[c].toString());
			}
		}
	}

	public void emptyHand(){
		for(int c = 0; c < MAXCARDS; c++){
			this.hand[c] = null;
		}
	}


}