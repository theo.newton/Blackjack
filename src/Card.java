/**
* An implementation of a card
*
* @author Theo Newton 
*
* @date 12 Oct 2015
*/
public class Card{
	
	private Suit cardSuit;
	/**
	*	Card number where A is 1, J-K is 11-13
	*/
	private int cardNumber;

	/**
	*	Card constructor
	*	@param s card suit
	*	@param n card number
	*/
	public Card(Suit s, int n){
		this.cardSuit = s;
		if(n >= 1 && n <= 13)
			this.cardNumber = n;
		else
			System.err.println(n + " is not a valid Card number");
	}

	/**
	*	Return card number
	*
	*	@return the number
	*/
	public int getNum(){
		return cardNumber;
	}		

	/**
	*	Override the toString()
	*/
	// MAYBE REFACTOR TO FETCH ARRAY POS?
	public String toString(){

		String numString = "";

		switch(this.cardNumber){
			case 1:
				numString = "Ace";
				break;
			case 2:
				numString = "Two";
				break;
			case 3: 
				numString = "Three";
				break;
			case 4:
				numString = "Four";
				break;
			case 5:
				numString = "Five";
				break;
			case 6:
				numString = "Six";
				break;
			case 7:
				numString = "Seven";
				break;
			case 8:
				numString = "Eight";
				break;
			case 9:
				numString = "Nine";
				break;
			case 10:				
				numString = "Ten";
				break;
			case 11:
				numString = "Jack";
				break;
			case 12:
				numString = "Queen";
				break;
			case 13:
				numString = "King";
				break;		
		}

		return numString + " of " + cardSuit.toString();
	}
}