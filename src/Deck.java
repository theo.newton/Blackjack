/**
* An implementation of a deck of cards
*
* @author Theo Newton 
*
* @date 12 Oct 2015
*/

import java.util.Random;

public class Deck{
	
	
	//Array of cards. Top card is at index 0
	private Card[] myCards; // Use ArrayList or LinkedList maybe -LL would make dealing easier???
	// Cards currently in the deck
	private int numCards;

	// Call the other constructor, 1 deck, no shuffle
	public Deck(){
		this(1, false);
	}

	/**
	*	Deck constructor
	*	@param numDeck Number of decks we will used
	*	@param shuffle Deck is shuffled or not
	*/
	public Deck(int numDecks, boolean shuffle) {
		// Find out how many cards are needed
		this.numCards = numDecks * 52;
		this.myCards = new Card[this.numCards];
		// Card index
		int c = 0;
		// For each deck
		for(int d = 0; d < numDecks; d++){
			// For each suit
			for(int s = 0; s < 4; s++){
				// For each number
				for(int n = 1; n <= 13; n++){
					// Add a new card to the deck
					this.myCards[c] = new Card(Suit.values()[s], n);
					c++;
				}
			}
		}

		// Shuffle if needed
		if(shuffle){
			this.shuffle();
		}
	}
	
	// Method to shuffle the deck
	public void shuffle(){
		// Create randNum generator

		Random rand = new Random();

		// Temporary card
		Card tmp;

		int j;
		for(int i = 0; i < this.numCards; i++){
			// Get rand card to swap with
			j = rand.nextInt(this.numCards);
			// Swap [IMPROVE???]
			tmp = this.myCards[i];
			this.myCards[i] = this.myCards[j];
			this.myCards[j] = tmp;
		}
	}
	
	/**
	*	Deal the next card
	*
	* @return the dealt card
	*/
	public Card dealNextCard(){
		// get top card
		;
		Card top = this.myCards[0];
		// shift all cards across by 1 index
		for(int c = 1; c < this.numCards; c++){
			this.myCards[c-1] = this.myCards[c];
		}
		this.myCards[this.numCards-1] = null;
		// Now 1 less card in the deck
		this.numCards--;
		return top;
	}

	// Print but don't remove
	public void printDeck(int numToPrint){
		for(int c = 0; c < numToPrint; c++){
			System.out.printf("% 3d/%d %s\n", c+1, this.numCards, this.myCards[c].toString());
		}
		System.out.printf("\t[%d others]\n", this.numCards - numToPrint);
	}
}